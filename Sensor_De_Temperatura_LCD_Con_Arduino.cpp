//Sensor De Temperatura LCD Con Arduino Y Tinkercard
/*
  LiquidCrystal Library 
  
 Este sketch imprime los valores de temperatura
 captados por el sensor.
 Se ve en la pantalla y el puerto serial.

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V

 */

// Incluye la libreria:
#include <LiquidCrystal.h>

//*****Pantalla LCD***********//
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 13, 5, 4, 3, 2);

int contrastPin = 6; //Pin que identifica el pin donde esta conectado el puerto de contraste del LCD
int contrastValue= 1; // Valor del contraste en la Pantalla (5-20)

//*****Sensor Temperatura*****//
int umbralTemp = 20; //Nuestro valor umbral seran 20 grados centigrados 
float sensorTemp = 0.0; //Lectura del sensor en valores decimales
int sensorPin = A0; //Configuracion del Pin de lectura del sensor de T
int temp = 0; //Lectura del sensor en valores enteros

//*****LED RGB***************//
int redLed = 9; 
int greenLed = 10;
int blueLed = 11;

void setup() {
  
  //***Puerto Serial***//
  //Seteo puerto serial para hacer display del sensor de Temperatura
  Serial.begin(9600);
  
  //***Pantalla LCD***//
  //Seteo el pin "contrast" del LCS
  pinMode(contrastPin,OUTPUT); //El pin contrast (LCD) debe estar configurado como salida
  // Seteo numero de filas y columnas:
  lcd.begin(16, 2);
  // Imprimo mensaje en el LCD.
  lcd.print("Temperatura: ");
  
  //***LED RGB********//
  pinMode(redLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
}

void loop() {
  
  //*********Sensor de Temperatura*************//
  int lectura = analogRead(sensorPin);
  //float voltaje = 5.0 /1024 * lectura ; // Atencion aqui
  // Si se usa un LM35 vuestra formula sera
  //float temp = voltaje * 100 ;
  //float sensorTemp = voltaje * 100 -50 ; 
  float sensorTemp = ((5.0 /1024) * lectura * 100) -50 ;
  Serial.print(sensorTemp);
  Serial.print(" ; "); 
  temp = (int)sensorTemp;
  Serial.println(temp); 
  //delay(100);
  //*******************************************//
  
  //*********LED´s***************************//
  if (temp > 35)
  {
    digitalWrite(redLed, HIGH);
    digitalWrite(greenLed, LOW);
    digitalWrite(blueLed, LOW);
  }
  else if (temp >=30 && temp<=35 )
  {
    digitalWrite(greenLed, HIGH);
    digitalWrite(redLed, LOW);
    digitalWrite(blueLed, LOW);
  }
  else if (temp < 30 )
  {
    digitalWrite(blueLed, HIGH);
    digitalWrite(redLed, LOW);
    digitalWrite(greenLed, LOW);
  }
  
  
  //*********Pantalla LCD*************//
  //Seteo el contraste
  analogWrite(contrastPin, contrastValue); //El pin contrast (LCD) se setea por valor analogico (5-20)
  // Seteo el cursor a columna 0, linea 1
  // (nota: linea 1 is la segunda fila, la primera fila es la 0):
  lcd.setCursor(6, 1);
  // print the number of seconds since reset:
  lcd.print(temp);
  
  if (temp >= 100)
  {
    lcd.setCursor(9, 1); // Posiciona el cursor para escribir
    lcd.print("C");//Simbolo de Centigrados
  }
  else if ((temp >= 0)&&(temp < 10))
  {
    lcd.setCursor(8, 1); //Borra los espacios que no se usan
    lcd.print(" ");
    lcd.setCursor(7, 1); //Ubica el simbolo de centigrados
    lcd.print("C");
  }
  else if ((temp >= -9)&&(temp < 0))
  {
    lcd.setCursor(9, 1); //Borra los espacios que no se usan
    lcd.print(" ");
    lcd.setCursor(8, 1); //Ubica el simbolo de centigrados
    lcd.print("C");
  }
  else if (temp < -10)
  {
    lcd.setCursor(9, 1); //Ubica el simbolo de centigrados
    lcd.print("C");
  }
  else
  {
    lcd.setCursor(9, 1);
    lcd.print(" ");
    lcd.setCursor(8, 1);
    lcd.print("C");
  }
  
  delay(500);
  //**********************************//
  
  
}